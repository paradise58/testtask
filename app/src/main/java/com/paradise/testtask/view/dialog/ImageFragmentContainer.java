package com.paradise.testtask.view.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;
import com.paradise.testtask.R;
import com.paradise.testtask.adapter.PhotoDetailAdapter;

public class ImageFragmentContainer extends DialogFragment {
  private static int position;

  public static ImageFragmentContainer getInstance(int pos) {
    position = pos;
    return new ImageFragmentContainer();
  }

  @Override public void onStart() {
    super.onStart();
    super.onStart();
    Dialog dialog = getDialog();
    if (dialog != null && dialog.getWindow() != null) {
      dialog.getWindow()
          .setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
      dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
  }

  @Nullable @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_photo_container, container, false);
    ViewPager viewPager = view.findViewById(R.id.view_pager);
    if (getActivity() != null) {
      PhotoDetailAdapter adapter = new PhotoDetailAdapter(getChildFragmentManager(),
          getActivity().getFilesDir().listFiles());
      viewPager.setAdapter(adapter);
      viewPager.setOffscreenPageLimit(3);
      viewPager.setCurrentItem(position);
    }
    viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override public void onPageScrolled(int i, float v, int i1) {

      }

      @Override public void onPageSelected(int position) {
      }

      @Override public void onPageScrollStateChanged(int i) {

      }
    });
    return view;
  }
}
