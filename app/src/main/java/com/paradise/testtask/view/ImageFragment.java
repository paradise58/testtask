package com.paradise.testtask.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.paradise.testtask.R;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class ImageFragment extends Fragment {
  public static final String ARGS_URI = "com.paradise.testthreerout.ARGS_URI";

  @Override public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    String uri = getArguments() != null ? getArguments().getString(ARGS_URI) : null;
    View view = inflater.inflate(R.layout.fragment_photo, container, false);
    view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
    PhotoView detailImageView = view.findViewById(R.id.photo_view);
    if (getActivity() != null) {
      Glide.with(getActivity())
          .load(uri)
          .fitCenter()
          .transition(withCrossFade())
          .into(detailImageView);
    }
    return view;
  }
}
