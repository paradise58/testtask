package com.paradise.testtask.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import com.paradise.testtask.R;

public class ActionDialog extends DialogFragment {
  private static Uri fileUri;
  private static int requestCode;
  private ActionDialogListener actionDialogListener;

  public static ActionDialog getInstance(Uri uri, int code) {
    fileUri = uri;
    requestCode = code;
    return new ActionDialog();
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    actionDialogListener = (ActionDialogListener) getActivity();
  }

  @NonNull @Override public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
    return new AlertDialog.Builder(requireContext()).setTitle(R.string.action_dialog_title)
        .setPositiveButton(R.string.action_dialog_crop, (v, d) -> {
          actionDialogListener.callCrop(fileUri);
        })
        .setNegativeButton(R.string.action_dialog_cancel, (dialog, which) -> {
        })
        .setNeutralButton(R.string.action_dialog_save, (v, d) -> {
          actionDialogListener.saveTask(requestCode, fileUri);
        })
        .create();
  }

  public interface ActionDialogListener {

    void callCrop(Uri uri);

    void saveTask(int requestCode, Uri uri);
  }

  @Override public void onDestroy() {
    super.onDestroy();
  }
}
