package com.paradise.testtask.view;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.paradise.testtask.R;
import com.paradise.testtask.adapter.RecyclerAdapter;
import com.paradise.testtask.app.App;
import com.paradise.testtask.view.dialog.ImageFragmentContainer;
import io.reactivex.disposables.Disposable;
import java.io.File;

public class StoredImagesFragment extends Fragment implements RecyclerAdapter.ViewHolderListener {

  private RecyclerAdapter adapter;
  private Disposable subjectDisposable;

  public StoredImagesFragment() {
  }

  @Override public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_stored_images, container, false);
    initRecyclerView(view);
    setAdapterData();
    subjectDisposable = App.getInstance().subject.subscribe(subjectData -> {
      if (subjectData.isComplete()) {
        setAdapterData();
        showToast(subjectData.getMessage());
      } else {
        showToast(subjectData.getMessage());
      }
    });
    return view;
  }

  private void initRecyclerView(View view) {
    RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
    LinearLayoutManager layoutManager =
        new LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false);
    recyclerView.setLayoutManager(layoutManager);
    adapter = new RecyclerAdapter(this);
    recyclerView.setAdapter(adapter);
  }

  @Override public void showFullSizeImage(int position) {
    ImageFragmentContainer.getInstance(position).show(getChildFragmentManager(), "PhotoDetail");
  }

  private void setAdapterData() {
    adapter.setData(getCurrentFiles());
  }

  private File[] getCurrentFiles() {
    if (getActivity() != null) {
      return getActivity().getFilesDir().listFiles();
    } else {
      return new File[] {};
    }
  }

  @Override public void deleteFile(Uri uri) {
    File file = new File(uri.getPath());
    boolean delete = file.delete();
    adapter.setData(getCurrentFiles());
    showToast("File delete " + (delete ? "done" : "failed"));
  }

  private void showToast(String message) {
    Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show();
  }

  @Override public void onDestroy() {
    super.onDestroy();
    if (subjectDisposable != null) subjectDisposable.dispose();
  }
}
