package com.paradise.testtask.view;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.google.android.material.snackbar.Snackbar;
import com.paradise.testtask.R;
import com.tbruyelle.rxpermissions2.RxPermissions;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import java.util.concurrent.atomic.AtomicInteger;

public class PickImageFragment extends Fragment {

  private Disposable camera_permission_observer;
  private MainActivity activity;
  private RxPermissions rxPermissions;

  public PickImageFragment() {
  }

  @Override public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    rxPermissions = new RxPermissions(this);
    activity = (MainActivity) getActivity();
    View view = inflater.inflate(R.layout.fragment_pick_image, container, false);
    initCameraButton(view);
    initGalleryButton(view);
    return view;
  }

  private void initCameraButton(View view) {
    Button cameraButton = view.findViewById(R.id.camera_button);
    cameraButton.setOnClickListener(v -> checkPermissionAndCallCamera(Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE));
  }

  private void initGalleryButton(View view) {
    Button galleryButton = view.findViewById(R.id.gallery_button);
    galleryButton.setOnClickListener(
        v -> checkPermissionAndCallCamera(Manifest.permission.READ_EXTERNAL_STORAGE));
  }

  private void checkPermissionAndCallCamera(String... requiredPermissions) {
    AtomicInteger permCount = new AtomicInteger();
    camera_permission_observer = rxPermissions.requestEach(requiredPermissions)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(permission -> {
          if (permission.granted) {
            if (activity.isFinishing()) return;
            permCount.getAndIncrement();
            if (permCount.get() == requiredPermissions.length) doAction(requiredPermissions[0]);
          } else if (permission.shouldShowRequestPermissionRationale) {
            if (activity.isFinishing()) return;
            showRequestPermissionRationale();
          } else {
            if (activity.isFinishing()) return;
            goToApplicationSettings();
          }
        }, Throwable::printStackTrace);
  }

  private void doAction(String requiredPermission) {
    switch (requiredPermission) {
      case Manifest.permission.CAMERA:
        activity.callCamera();
        break;
      case Manifest.permission.READ_EXTERNAL_STORAGE:
        activity.callGallery();
        break;
    }
  }

  private void showRequestPermissionRationale() {
    Snackbar snackbar = activity.getSnackBar();
    snackbar.setText(R.string.rational);
    snackbar.setAction(R.string.snack_bar_ok, v -> snackbar.dismiss());
    snackbar.show();
  }

  private void goToApplicationSettings() {
    Snackbar snackbar = activity.getSnackBar();
    snackbar.setText(R.string.open_settings).setAction(R.string.settings, view -> {
      Intent intent = new Intent();
      intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
      Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
      intent.setData(uri);
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      startActivity(intent);
    });
    snackbar.show();
  }

  @Override public void onDestroy() {
    super.onDestroy();
    if (camera_permission_observer != null) camera_permission_observer.dispose();
  }
}
