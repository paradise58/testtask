package com.paradise.testtask.view;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.LinearLayout;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.snackbar.Snackbar;
import com.paradise.testtask.R;
import com.paradise.testtask.app.App;
import com.paradise.testtask.app.SubjectData;
import com.paradise.testtask.view.dialog.ActionDialog;
import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.Subject;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import org.apache.commons.io.FileUtils;

public class MainActivity extends AppCompatActivity implements ActionDialog.ActionDialogListener {
  public static final String PREFIX_CAMERA = "PARADISE_CAMERA";
  public static final String PREFIX_GALLERY = "PARADISE_GALLERY";
  public static final String PREFIX_CROP = "PARADISE_CROP";
  public static final int CAMERA_REQUEST = 0;
  public static final int GALLERY_REQUEST = 1;
  public static final int CROP_REQUEST = 2;

  Subject<SubjectData> subject = App.getInstance().subject;
  Snackbar snackbar;
  Uri cacheImageUri;
  LinearLayout mainScreenView;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    initToolBar();
    mainScreenView = findViewById(R.id.main_screen);
    snackbar = Snackbar.make(mainScreenView, "", Snackbar.LENGTH_SHORT);
    ViewPager viewPager = findViewById(R.id.view_pager);
    viewPager.setAdapter(createViewPagerAdapter());
  }

  public Snackbar getSnackBar() {
    return snackbar;
  }

  private void initToolBar() {
    Toolbar toolbar = findViewById(R.id.tool_bar);
    setSupportActionBar(toolbar);
  }

  private FragmentPagerAdapter createViewPagerAdapter() {
    return new FragmentPagerAdapter(getSupportFragmentManager()) {

      @Override public Fragment getItem(int position) {
        switch (position) {
          case 0:
            return new PickImageFragment();
          case 1:
            return new StoredImagesFragment();
          default:
            return null;
        }
      }

      @Override public int getCount() {
        return 2;
      }

      @Override public CharSequence getPageTitle(int position) {
        switch (position) {
          case 0:
            return getString(R.string.first_tab_item);
          case 1:
            return getString(R.string.second_tab_item);
          default:
            return getString(R.string.wtf);
        }
      }
    };
  }

  public void callCamera() {
    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    File cacheImage = createCacheFile();
    cacheImageUri = FileProvider.getUriForFile(this, getPackageName() + ".provider", cacheImage);
    cameraIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, cacheImageUri);
    startActivityForResult(cameraIntent, CAMERA_REQUEST);
  }

  private File createCacheFile() {
    String imageFileName = "jpeg" + String.valueOf(new Date().getTime());
    File storageDir = getCacheDir();
    File cacheFile = null;
    try {
      cacheFile = File.createTempFile(imageFileName, ".jpg", storageDir);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return cacheFile;
  }

  public void callGallery() {
    Intent galleryIntent = new Intent(Intent.ACTION_PICK);
    galleryIntent.setType("image/*");
    startActivityForResult(galleryIntent, GALLERY_REQUEST);
  }

  @Override public void callCrop(Uri pickedImageUri) {
    File image = createImageFile(CROP_REQUEST);
    Uri imageUri = FileProvider.getUriForFile(this, getPackageName() + ".provider", image);
    Intent cropIntent = new Intent("com.android.camera.action.CROP");
    cropIntent.setDataAndType(pickedImageUri, "image/*");
    cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
    List<ResolveInfo> resolveActivityList =
        getPackageManager().queryIntentActivities(cropIntent, PackageManager.MATCH_DEFAULT_ONLY);
    grantPermissions(resolveActivityList, imageUri, pickedImageUri);
    startActivityForResult(
        Intent.createChooser(cropIntent, getString(R.string.call_crop_chooser_title)),
        CROP_REQUEST);
  }

  private void grantPermissions(List<ResolveInfo> resolveActivityList, Uri imageUri,
      Uri pickedImageUri) {
    for (ResolveInfo resolveInfo : resolveActivityList) {
      grantUriPermission(resolveInfo.activityInfo.packageName, imageUri,
          Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
      grantUriPermission(resolveInfo.activityInfo.packageName, pickedImageUri,
          Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
    }
  }

  private File createImageFile(int requesCode) {
    String prefix = getPrefix(requesCode);
    StringBuilder imageFileName =
        new StringBuilder(prefix).append(String.valueOf(new Date().getTime()));
    File storageDir = getFilesDir();
    File imageFile = null;
    try {
      imageFile = File.createTempFile(imageFileName.toString(), ".jpg", storageDir);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return imageFile;
  }

  String getPrefix(int requestCode) {
    switch (requestCode) {
      case CAMERA_REQUEST:
        return PREFIX_CAMERA;
      case GALLERY_REQUEST:
        return PREFIX_GALLERY;
      case CROP_REQUEST:
        return PREFIX_CROP;
      default:
        return "null";
    }
  }

  @Override public void saveTask(int requestCode, Uri pickedUri) {
    Disposable subscribe = Completable.fromAction(() -> {
      if (requestCode != CROP_REQUEST) saveImageToAppStorage(pickedUri, requestCode);
    }).subscribe(() -> {
      if (!isFinishing()) {
        subject.onNext(new SubjectData(true, getString(R.string.save_complete)));
      }
      clearCache();
    }, e -> subject.onNext(new SubjectData(false, getString(R.string.save_failed))));
  }

  private void saveImageToAppStorage(Uri pickedUri, int requestCode) {
    File pickedImage;
    if (requestCode == GALLERY_REQUEST) {
      pickedImage = new File(getPathFromUri(pickedUri));
    } else {
      pickedImage = FileUtils.getFile(getCacheDir().getParent(), pickedUri.getPath());
    }
    save(pickedImage, requestCode);
  }

  private void save(File pickedImage, int requestCode) {
    File image = createImageFile(requestCode);
    try {
      FileUtils.copyFile(pickedImage, image);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void clearCache() {
    if (getCacheDir() != null) {
      File[] files = getCacheDir().listFiles();
      for (File file : files) {
        if (file.exists() && file.canWrite()) //noinspection ResultOfMethodCallIgnored
          file.delete();
      }
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK) {
      switch (requestCode) {
        case CAMERA_REQUEST:
          showActionDialog(cacheImageUri, CAMERA_REQUEST);
          break;
        case GALLERY_REQUEST:
          if (data != null) {
            Uri pickedImageUri = data.getData();
            showActionDialog(pickedImageUri, GALLERY_REQUEST);
          }
          break;
        case CROP_REQUEST:
          saveTask(CROP_REQUEST, null);
          break;
      }
    }
  }

  void showActionDialog(Uri uri, int requestCode) {
    ActionDialog.getInstance(uri, requestCode).show(getSupportFragmentManager(), "ActionDialog");
  }

  private String getPathFromUri(Uri uri) {
    String data = MediaStore.Images.Media.DATA;
    String[] filePathColumn = { data };
    String path;
    Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
    if (cursor != null) {
      cursor.moveToFirst();
      int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
      path = cursor.getString(columnIndex);
      cursor.close();
    } else {
      path = uri.getPath();
    }
    return path;
  }

  @Override public void onBackPressed() {
    showExitDialog();
  }

  void showExitDialog() {
    new AlertDialog.Builder(this).setTitle(getString(R.string.close_dialog_title))
        .setNeutralButton(R.string.close_dialog_yes, (v, d) -> super.onBackPressed())
        .setPositiveButton(R.string.close_dialog_no, null)
        .create()
        .show();
  }
}