package com.paradise.testtask.adapter;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.paradise.testtask.R;
import com.paradise.testtask.view.MainActivity;
import java.io.File;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {
  private ViewHolderListener viewHolderListener;
  private File[] imagesList;

  public interface ViewHolderListener {

    void showFullSizeImage(int imageUri);

    void deleteFile(Uri uri);
  }

  public RecyclerAdapter(ViewHolderListener viewHolderListener) {
    this.viewHolderListener = viewHolderListener;
  }

  public void setData(File[] listFiles) {
    imagesList = listFiles;
    notifyDataSetChanged();
  }

  @NonNull @Override
  public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false);
    MyViewHolder viewHolder = new MyViewHolder(view);
    view.setOnClickListener(viewHolder);
    view.setOnLongClickListener(viewHolder);
    return viewHolder;
  }

  @Override public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
    holder.bindView(imagesList[position]);
  }

  @Override public int getItemCount() {
    return imagesList == null ? 0 : imagesList.length;
  }

  class MyViewHolder extends RecyclerView.ViewHolder
      implements View.OnClickListener, View.OnLongClickListener {
    TextView imageName;
    ImageView image;
    Uri uri;

    MyViewHolder(@NonNull View itemView) {
      super(itemView);
      imageName = itemView.findViewById(R.id.item_text);
      image = itemView.findViewById(R.id.item_image);
    }

    void bindView(File file) {
      uri = Uri.fromFile(file);
      Glide.with(image.getContext()).load(Uri.fromFile(file)).thumbnail(0.1f).into(image);
      StringBuilder imageName =
          new StringBuilder(getPrefix(file)).append("image № ").append(getAdapterPosition());
      this.imageName.setText(imageName);
    }

    String getPrefix(File file) {
      String path = file.getPath();
      if (path.contains(MainActivity.PREFIX_CAMERA)) {
        return "Camera ";
      } else if (path.contains(MainActivity.PREFIX_GALLERY)) {
        return "Gallery ";
      } else if (path.contains(MainActivity.PREFIX_CROP)) return "Crop ";
      return "null";
    }

    @Override public void onClick(View v) {
      viewHolderListener.showFullSizeImage(getAdapterPosition());
    }

    @Override public boolean onLongClick(View v) {
      viewHolderListener.deleteFile(uri);
      return true;
    }
  }
}