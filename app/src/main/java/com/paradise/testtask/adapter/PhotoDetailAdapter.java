package com.paradise.testtask.adapter;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import com.paradise.testtask.view.ImageFragment;
import java.io.File;

public class PhotoDetailAdapter extends FragmentPagerAdapter {
  private File[] photos;

  public PhotoDetailAdapter(FragmentManager fm, File[] filesDir) {
    super(fm);
    photos = filesDir;
  }

  @Override public Fragment getItem(int position) {
    Bundle bundle = new Bundle();
    bundle.putString(ImageFragment.ARGS_URI, photos[position].getPath());
    ImageFragment fragment = new ImageFragment();
    fragment.setArguments(bundle);
    return fragment;
  }

  @Override public int getCount() {
    return photos.length;
  }
}