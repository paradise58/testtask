package com.paradise.testtask.app;

public class SubjectData {
  private boolean isComplete;
  private String message;

  public SubjectData(boolean isComplete, String message) {
    this.isComplete = isComplete;
    this.message = message;
  }

  public boolean isComplete() {
    return isComplete;
  }

  public String getMessage() {
    return message;
  }
}
