package com.paradise.testtask.app;

import android.app.Application;
import io.reactivex.subjects.PublishSubject;

public class App extends Application {
  public static App instance;

  public PublishSubject<SubjectData> subject = PublishSubject.create();

  @Override public void onCreate() {
    super.onCreate();
    instance = this;
  }

  public static App getInstance() {
    return instance;
  }
}
